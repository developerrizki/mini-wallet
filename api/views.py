from datetime import datetime
import uuid,random
from django.shortcuts import render
from rest_framework import viewsets
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework.permissions import IsAuthenticated
from rest_framework.authtoken.models import Token
from django.contrib.auth.models import User
from django.contrib.auth.hashers import make_password
from .models import Customer, Wallet, Deposit, Withdrawal
from . import models, serializers
class CustomerViewSet(viewsets.ModelViewSet):
    queryset = models.Customer.objects.all()
    serializer_class = serializers.CustomerSerializer

class InitView(APIView):
    def post(self, request):
        customer_xid = request.POST["customer_xid"]
        if (customer_xid):

            #create user
            user = User.objects.create(
                username="cs" + customer_xid,
                is_superuser=0,
                is_staff=1,
                is_active=1,
                email=customer_xid + "@gmail.com",
                password=make_password("1234567890"),
                first_name="Customer",
                last_name=customer_xid,
                date_joined=datetime.now()
            )

            # create token
            token = Token.objects.create(user=user)

            # create customer
            Customer.objects.get_or_create(
                customer_xid=customer_xid,
                user_id=user.id,
                token=token.key
            )

            response = {
                'data': {
                    'token': token.key
                },
                'status': 'success'
            }
            return Response(response, 201)
        else :
            error = {
                'data': {
                    'error': {
                        'customer_xid': [
                            'Missing data for required field.'
                        ]
                    }
                }, 'status' : 'fail'
            }
            return Response(error, 400)

class WalletView(APIView):
    permission_classes = (IsAuthenticated,)

    def get(self, request):
        token = request.auth.key

        # check customer
        customer = Customer.objects.filter(token=token).latest('id')

        # check existing wallet
        wallet = Wallet.objects.filter(owned_by_id=customer.id, status="enabled").first()

        if (wallet):
            response = {
                'status': 'success',
                'data': {
                    'wallet': {
                        'id': wallet.slug,
                        'owned_by': wallet.owned_by.customer_xid,
                        'status': wallet.status,
                        'enabled_at': wallet.enable_at,
                        'balance': wallet.balance
                    }
                }
            }
            return Response(response, 200)
        else:
            error = {
                'status': 'fail',
                'data': {
                    'error': 'Disabled'
                }
            }
            return Response(error, 404)

    def post(self, request):
        token = request.auth.key

        # check customer
        customer = Customer.objects.filter(token=token).latest('id')

        # check existing wallet
        wallet = Wallet.objects.filter(owned_by_id=customer.id).first()

        if (wallet):
            error = {
                'status': 'fail',
                'data': {
                    'error': 'Already enabled'
                }
            }
            return Response(error, 400)
        else:
            # create new wallet
            wallet = Wallet.objects.create(
                slug=uuid.uuid1(random.randint(0, 281474976710655)),
                owned_by_id=customer.id,
                status="enabled",
                enable_at=datetime.now(),
                balance=0
            )

            response = {
                'status': 'success',
                'data': {
                    'wallet': {
                        'id': wallet.slug,
                        'owned_by': wallet.owned_by.customer_xid,
                        'status': wallet.status,
                        'enabled_at': wallet.enable_at,
                        'balance': wallet.balance
                    }
                }
            }
            return Response(response, 201)

    def patch(self, request):
        token = request.auth.key
        is_disabled = request.POST["is_disabled"]

        # check customer
        customer = Customer.objects.filter(token=token).latest('id')

        # check existing wallet
        wallet = Wallet.objects.filter(owned_by_id=customer.id).first()
        
        if (wallet):

            if (is_disabled == "true") :
                Wallet.objects.filter(owned_by_id=customer.id).update(status="disabled")
            elif(is_disabled == "false"):
                Wallet.objects.filter(owned_by_id=customer.id).update(status="enabled")

            wallet = Wallet.objects.filter(owned_by_id=customer.id).first()

            response = {
                'status': 'success',
                'data': {
                    'wallet': {
                        'id': wallet.slug,
                        'owned_by': wallet.owned_by.customer_xid,
                        'status': wallet.status,
                        'enabled_at': wallet.enable_at,
                        'balance': wallet.balance
                    }
                }
            }
            return Response(response, 200)
        else:
            error = {
                'status': 'fail',
                'data': {
                    'error': 'Disabled'
                }
            }
            return Response(error, 404)
class DepositView(APIView):
    permission_classes = (IsAuthenticated,)

    def post(self, request):
        token = request.auth.key
        amount = int(request.POST['amount'])
        reference_id = request.POST['reference_id']

        # check customer & wallet
        customer = Customer.objects.filter(token=token).latest('id')
        wallet = Wallet.objects.filter(owned_by_id=customer.id, status="enabled").first()

        if (wallet) :
            deposit = Deposit.objects.create(
                slug=uuid.uuid1(random.randint(0, 281474976710655)),
                deposited_by_id=customer.id,
                status="success",
                deposited_at=datetime.now(),
                amount=amount,
                reference_id=reference_id
            )

            # update balance
            Wallet.objects.filter(owned_by_id=customer.id).update(
                balance=wallet.balance + amount)

            response = {
                'status': 'success',
                'data': {
                    'deposit': {
                        'id': deposit.slug,
                        'deposited_by': deposit.deposited_by.customer_xid,
                        'status': deposit.status,
                        'deposited_at': deposit.deposited_at,
                        'amount': deposit.amount,
                        'reference_id': deposit.reference_id,
                    }
                }
            }
            return Response(response, 200)
        else:
            error = {
                'status': 'fail',
                'data': {
                    'error': 'Your wallet are disabled'
                }
            }
            return Response(error, 404)
class WithdrawalView(APIView):
    permission_classes = (IsAuthenticated,)

    def post(self, request):
        token = request.auth.key
        amount = int(request.POST['amount'])
        reference_id = request.POST['reference_id']

        # check customer & wallet
        customer = Customer.objects.filter(token=token).latest('id')
        wallet = Wallet.objects.filter(owned_by_id=customer.id, status="enabled").first()

        #check status wallet
        if (wallet) :
            # check balance
            if (amount > wallet.balance):
                error = {
                    'status': 'fail',
                    'data': {
                        'error': 'Your balance not enough'
                    }
                }
                return Response(error, 404)
            else:
                withdrawal = Withdrawal.objects.create(
                    slug=uuid.uuid1(random.randint(0, 281474976710655)),
                    withdrawn_by_id=customer.id,
                    status="success",
                    withdrawn_at=datetime.now(),
                    amount=amount,
                    reference_id=reference_id
                )

                # update balance
                Wallet.objects.filter(owned_by_id=customer.id).update(
                    balance=wallet.balance - amount)

                response = {
                    'status': 'success',
                    'data': {
                        'withdrawal': {
                            'id': withdrawal.slug,
                            'withdrawn_by': withdrawal.withdrawn_by.customer_xid,
                            'status': withdrawal.status,
                            'withdrawn_at': withdrawal.withdrawn_at,
                            'amount': withdrawal.amount,
                            'reference_id': withdrawal.reference_id,
                        }
                    }
                }
                return Response(response, 200)
        else :
            error = {
                'status': 'fail',
                'data': {
                    'error': 'Your wallet are disabled'
                }
            }
            return Response(error, 404)
