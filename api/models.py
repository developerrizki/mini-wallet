from ast import mod
from cgitb import enable
from django.db import models
from django.contrib.auth.models import User

class Customer(models.Model):
    customer_xid = models.CharField(max_length=254)
    token = models.TextField(null=True, blank=True)
    user = models.ForeignKey(User, on_delete=models.CASCADE)

    def __str__(self):
        return self.customer_xid

class Wallet(models.Model):
    slug = models.SlugField(max_length=100)
    owned_by = models.ForeignKey(Customer, on_delete=models.CASCADE)
    status = models.CharField(max_length=20)
    enable_at = models.DateTimeField(null=True, blank=True)
    balance = models.IntegerField(default=0)

    def __str__(self):
        return self.slug

class Deposit(models.Model):
    slug = models.SlugField(max_length=100)
    deposited_by = models.ForeignKey(Customer, on_delete=models.CASCADE)
    status = models.CharField(max_length=20)
    deposited_at = models.DateTimeField(null=True, blank=True)
    amount = models.IntegerField(default=0)
    reference_id = models.SlugField(max_length=100)

    def __str__(self):
        return self.slug

class Withdrawal(models.Model):
    slug = models.SlugField(max_length=100)
    withdrawn_by = models.ForeignKey(Customer, on_delete=models.CASCADE)
    status = models.CharField(max_length=20)
    withdrawn_at = models.DateTimeField(null=True, blank=True)
    amount = models.IntegerField(default=0)
    reference_id = models.SlugField(max_length=100)

    def __str__(self):
        return self.slug