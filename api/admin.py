from uuid import uuid4
from django.contrib import admin
from . import models

class CustomerAdmin(admin.ModelAdmin):
    list_display = ('customer_xid', 'token')

class WalletAdmin(admin.ModelAdmin):
    list_display = ('slug', 'owned_by', 'status', 'enable_at', 'balance')

class DepositAdmin(admin.ModelAdmin):
    list_display = ('slug', 'deposited_by', 'status', 'deposited_at', 'amount', 'reference_id')

class WithdrawalAdmin(admin.ModelAdmin):
    list_display = ('slug', 'withdrawn_by', 'status', 'withdrawn_at', 'amount', 'reference_id')

admin.site.register(models.Customer, CustomerAdmin)
admin.site.register(models.Wallet, WalletAdmin)
admin.site.register(models.Deposit, DepositAdmin)
admin.site.register(models.Withdrawal, WithdrawalAdmin)
