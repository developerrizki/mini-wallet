from django.urls import include, path
from rest_framework import routers
from . import views

router = routers.DefaultRouter()
router.register(r'customer', views.CustomerViewSet)

# Wire up our API using automatic URL routing.
# Additionally, we include login URLs for the browsable API.
urlpatterns = [
    path('', include(router.urls)),
    path('init', views.InitView.as_view()),
    path('wallet', views.WalletView.as_view()),
    path('wallet/deposits', views.DepositView.as_view()),
    path('wallet/withdrawals', views.WithdrawalView.as_view()),
    path('api-auth/', include('rest_framework.urls', namespace='rest_framework'))
]
