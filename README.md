# Prepare Environment
1. Download & Install python
    - https://www.python.org/downloads/
2. Install pip
    - Mac OS : https://www.geeksforgeeks.org/how-to-install-pip-in-macos/
    - Windows : https://www.geeksforgeeks.org/how-to-install-pip-on-windows/
3. Install pipenv
    - python2 : pip install pipenv
    - python3 : pip3 install pipenv

# Recommended IDE
1. Install VSCode : https://code.visualstudio.com/download
2. Install VSCode Extenstion : Python (https://marketplace.visualstudio.com/items?itemName=ms-python.python)

# Prepare Start Project
1. mkdir miniwallet
2. cd miniwallet
3. Install Django run this script inside miniwallet folder
    - pipenv install django
4. Install Django Rest Framework run this script inside miniwallet folder
    - pipenv install djangoresframework
5. Copy path of venv after install *pipenv install django* or you can run again *pipenv shell*
6. Open folder miniwallet on VSCode
7. Click View > Command Palette > Search *Python: Select Interpreter*
8. Copy path venv to this, and enter
9. Open Terminal on VSCode. (Click View > Terminal)
10. Run makemigrations : 
    - python2 : python manage.py makemigrations
    - python3 : python3 manage.py makemigrations 
11. Run migrate : 
    - python2 : python manage.py migrate
    - python3 : python3 manage.py migrate 
12. Run local server (localhost:8000) :
    - python2 : python manage.py runserver
    - python3 : python3 manage.py runserver 

# Postman
- https://www.getpostman.com/collections/3fc0569a4630a373013c
